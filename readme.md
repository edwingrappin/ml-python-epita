Welcome to Machine Learning with Python Part I! 

You'll find in this repository some materials for the course. 

To benefit plenty from the code, you will need to install docker and build and run the configured image. It will ensure that all dependency are installed when you use the code and the JupyterLab sessions. 

If you have any question about the course, feel free to reach out via mail or within gitlab. 

## Run docker and use jupyter lab. 
It's pretty simple. 
From the folder `Conf`, run in command line:
`sudo ./build.sh`, it will build (if not already done) and run a configured container with Jupyter Lab that you can reach locally in you favorite navigator. 

In the command line window where you ran `sudo ./build.sh`, a URL of the format `http://127.0.0.1:8888/?token=41ed316c2c1ac462` will be displayed, copy paste it in your URL, and change the port from `8888` to `32788`. Jupyter Lab should be up and running. 

In `/app/Repo/ml-python-epita` you'll find the materials of the course. 

## Data Challenge for Evaluation
In order to run the code within the docker configuration. 
You need the following folder architecture:

- ./Repo/ml-python-epita --> the repo of the code
- ./Data/LearningDataSet --> the folder with the data for the challenges

Then, within the container, you can access your folders in the following directories:
- /app/Data/
- /app/Repo/

Then in /app/Data/LearningDataSet the data is organized by modality of experiment. One folder is on experiment. Then in each folder, the data is stored in subfolders called "spectre". In these folders, the convention is the following:
12-1-002.pj2 --> means that this is the modality 12, the first replicate and the rank 2.
One file is one NMR spectre. The first column is the time of relaxation and the second column is the density of the signal corresponding to the time of relaxation.




